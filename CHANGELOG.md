# Dev Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- netlify.toml
- archetypes/default.md
- cypress.config.js
  - cypress/e2e/basic.cy.js
- netlify.toml
- package.json
- LICENSE.md

### Changed

- CHANGELOG.md

### Removed

- LICENSE

## [0.0.4] - 2024-02-27

### Added

- netlify.toml
- archetypes/default.md
- cypress.config.js
  - cypress/e2e/basic.cy.js
- netlify.toml
- package.json
- LICENSE.md

### Changed

- CHANGELOG.md

### Removed

- LICENSE
- themes/Lanyon
- themes/beautifulhugo

  
## [0.0.3] - 2024-02-25

### Added

- Better explanation of the difference between the file ("CHANGELOG")
  and its function "the change log".

### Changed

- Refer to a "change log" instead of a "CHANGELOG" throughout the site
  to differentiate between the file and the purpose of the file — the
  logging of changes.

## [0.0.2] - 2024-02-25

### Added

- Added new "themes/davit/" based a little from "beutifulhugo" as a place marker.

### Changed

- Modified **config.toml** to reflect new theme
- content/page/about.md

## [0.0.1] - 2024-02-25

### Added

- This [CHANGELOG](CHANGELOG.md) file to hopefully serve as an evolving example of a
  standardized open source project CHANGELOG.
- Original Template from [Hugo Quickstart Template](https://github.com/netlify-templates/hugo-quickstart)
- .vscode - settings

### Modified

- .gitignore
- CHANGELOG.md
- config.toml


[unreleased]: https://gitlab.com/DavitTec/dev/compare/v0.0.2...HEAD
[0.0.2]: https://gitlab.com/DavitTec/dev/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/DavitTec/dev/-/tags/v0.0.1
